<div class="container">
    <div class="row mt-3">
        <div class="col-md-6">

        <div class="card">
            <div class="card-header">
                Featured
            </div>
            <div class="card-body">
                <h5 class="card-title"><?= $customer['nama'] ?></h5>
                <h6 class="card-subtitle mb-2 text-muted"><?= $customer['handphone'] ?></h6>
                <p class="card-text"><?= $customer['elektronik'] ?></p>
                <p class="card-text"><?= $customer['kerusakan'] ?></p>
                <p class="card-text"><?= $customer['keterangan'] ?></p>
                <a href="<?= base_url();?>mahasiswa" class="btn btn-primary">Kembali</a>
            </div>
        </div>

        </div>
    </div>
</div>