<div class="container">
    <div class="row mt-3">
        <div class="col-md-6">
            <div class="card">
            <div class="card-header">
                Form Ubah Data Cutomer
            </div>
            <div class="card-body">
                <!-- <?php  if (validation_errors () ) : ?>  
                <div class="alert alert-danger" role="alert">
                    <?= validation_errors(); ?>
                </div>
                <?php endif; ?> -->

                <form action="" method ="POST">
                    <div class="form-group">
                        <label for="id_pelanggan">ID</label>
                        <input type="text" class="form-control" id="id_pelanggan" name = "id_pelanggan" value="<?= $customer['id_pelanggan']; ?>">   
                        <small class="form-text text-danger"><?= form_error('id_pelanggan'); ?></small>
                    </div>
                    <div class="form-group">
                        <label for="nama">Nama</label>
                        <input type="text" class="form-control" id="nama" name = "nama" value="<?= $customer['nama']; ?>">   
                        <small class="form-text text-danger"><?= form_error('nama'); ?></small>
                    </div>
                    <div class="form-group">
                        <label for="handphone">Handphone</label>
                        <input type="text" class="form-control" id="handphone" name= "handphone" value="<?= $customer['handphone']; ?>">
                        <small class="form-text text-danger"><?= form_error('handphone'); ?></small>
                    </div>
                    <div class="form-group">
                        <label for="elektronik">Elektronik</label>
                        <input type="text" class="form-control" id="elektronik" name= "elektronik" value="<?= $customer['elektronik']; ?>">
                        <small class="form-text text-danger"><?= form_error('elektronik'); ?></small>
                    </div>
                    <div class="form-group">
                        <label for="tanggal">Tanggal Daftar</label>
                        <input type="text" class="form-control" id="tanggal" name= "tanggal" value="<?= $customer['tanggal']; ?>">
                        <small class="form-text text-danger"><?= form_error('tanggal'); ?></small>
                    </div>
                    <div class="form-group">
                        <label for="last_update">Tanggal Pengerjaan</label>
                        <input type="text" class="form-control" id="last_update" name= "last_update" value="<?= $customer['last_update']; ?>">
                        <small class="form-text text-danger"><?= form_error('last_update'); ?></small>
                    </div>
                    <div class="form-group" >
                        <label for="status">Status</label>
                        <select class="form-control" id="status" name="status">
                            <?php foreach ($status as $sa ) : ?>
                                <?php if( $sa == $customer['status'] ) : ?>
                                    <option value="<?= $sa; ?>" selected><?= $sa; ?></option>
                                <?php else :  ?>
                                    <option value="<?= $sa; ?>" ><?= $sa; ?></option>
                                <?php endif; ?>
                            <?php endforeach; ?>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="kerusakan">Kerusakan</label>
                        <input type="text" class="form-control" id="kerusakan" name= "kerusakan" value="<?= $customer['kerusakan']; ?>">
                    </div>
                    <div class="form-group">
                        <label for="keterangan">Keterangan</label>
                        <input type="text" class="form-control" id="keterangan" name= "keterangan" value="<?= $customer['keterangan']; ?>">
                    </div>
                    <button class="btn btn-primary float-right" type="submit"  name="ubah"> Ubah data</button>
                    <a href="<?= base_url() ?>/mahasiswa" class="btn btn-warning float-left" type="submit"  name="ubah"> Kembali</a>
                </form>
            </div>
            </div>
        </div>
    </div>
</div>