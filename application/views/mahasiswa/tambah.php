<div class="container">
    <div class="row mt-3">
        <div class="col-md-6">
            <div class="card">
            <div class="card-header">
                Form Tambah Data Cutomer
            </div>
            <div class="card-body">
                <!-- <?php  if (validation_errors () ) : ?>  
                <div class="alert alert-danger" role="alert">
                    <?= validation_errors(); ?>
                </div>
                <?php endif; ?> -->

                <form action="" method ="POST">
                    <div class="form-group">
                        <label for="nama">Nama</label>
                        <input type="text" class="form-control" id="nama" name = "nama" >   
                        <small class="form-text text-danger"><?= form_error('nama'); ?></small>
                    </div>
                    <div class="form-group" style = display:none;>
                        <label for="id_pelanggan">ID Pelanggan</label>
                        <input type="text" class="form-control" id="id_pelanggan" name = "id_pelanggan" >   
                    </div>
                    <div class="form-group">
                        <label for="handphone">Handphone</label>
                        <input type="text" class="form-control" id="handphone" name= "handphone" >
                        <small class="form-text text-danger"><?= form_error('handphone'); ?></small>
                    </div>
                    <div class="form-group">
                        <label for="elektronik">Elektronik</label>
                        <input type="text" class="form-control" id="elektronik" name= "elektronik" >
                        <small class="form-text text-danger"><?= form_error('elektronik'); ?></small>
                    </div>
                    <div class="form-group">
                        <label for="kerusakan">Kerusakan</label>
                        <input type="text" class="form-control" id="kerusakan" name= "kerusakan" >
                    </div>
                    <div class="form-group">
                        <label for="keterangan">Keterangan</label>
                        <input type="text" class="form-control" id="keterangan" name= "keterangan" >
                    </div>
               
                    <button class="btn btn-primary float-right" type="submit"  name="tambah"> Tambah data</button>
                </form>
            </div>
            </div>
        </div>
    
    
    </div>


</div>