<div class="container">
    <h3 class= "mt-3">List Of Peoples</h3>

    
    <div class="row">
        <div class="col-md-5">
            <form action="<?= base_url('peoples'); ?>" method="POST">
                <div class="input-group mb-3">
                    <input type="text" class="form-control" placeholder="Search any Keywords" autocomplete= "off" name ="keyword" autofocus
                    >
                    <div class="input-group-prepend">
                        <input type="submit" name = "submit" class="btn btn-primary">
                    </div>
                </div>
            </form>

        </div>
    </div>

    <div class="row">
        <div class="col-md-10">
            <h5>Result : <?= $total_rows; ?> </h5>
            <table class="table">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Name</th>
                        <th>Email</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    <?php if (empty($peoples)) : ?>
                        <tr>
                            <td colspan ="4">
                                <div class="alert alert-danger" role="alert">
                                    Data not found ..!
                                </div>
                            </td>
                        </tr>
                    <?php endif; ?>
                    <?php  foreach ($peoples as $peoples) : ?>
                    <tr>
                        <td> <?php echo ++$start; ?> </td>
                        <td><?php echo $peoples ['name']; ?></td>
                        <td><?php echo $peoples ['email']; ?></td>
                        <td>
                            <a href="<?= base_url() ?>people/hapus/" class ="badge badge-danger " onclick="return confirm('Anda Yakin?')">hapus</a>
                            <a href="<?= base_url() ?>people/ubah/" class ="badge badge-success " >Ubah</a>
                            <a href="<?= base_url() ?>people/detail/" class ="badge badge-primary" >Detail</a>
                        </td>
                    </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
            <?=  $this->pagination->create_links(); ?>



        </div>
    </div>
</div>