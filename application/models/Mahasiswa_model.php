<?php 
    
    class Mahasiswa_model extends CI_model {

        public function getAllMahasiswa ()
        {
            return $this->db->get('customer')->result_array();
        }

        public function tambahDataMahasiswa ()
        {
            $data = [
                "nama" => $this->input->post('nama', true),
                "id_pelanggan" => $this->input->post('id_pelanggan', true),
                "handphone" => $this->input->post('handphone', true),
                "elektronik" => $this->input->post('elektronik', true),
                "kerusakan" => $this->input->post('kerusakan', true),
                "keterangan" => $this->input->post('keterangan', true)


            ];

            $this ->db->insert('customer', $data);
        }

        public function hapusMahasiswaData ($id_pelanggan)
        {
            $this->db->where('id_pelanggan', $id_pelanggan);
            
            $this->db->delete('customer');
        }

        public function getMahasiswaById ($id_pelanggan)
        {
            return $this->db->get_where('customer', ['id_pelanggan' => $id_pelanggan]) ->row_array();
            //  cara kedua seperti hapus data / memanggil array 
            
            
        }


        public function ubahDataMahasiswa ()
        {
            $data = 
            [
                "nama" => $this->input->post('nama', true),
                "handphone" => $this->input->post('handphone', true),
                "elektronik" => $this->input->post('elektronik', true),
                "kerusakan" => $this->input->post('kerusakan', true),
                "keterangan" => $this->input->post('keterangan', true)


            ];

        
                $this->db->where('id_pelanggan', $this->input->post('id_pelanggan'));
                $this->db->update('customer', $data);
                
        }

        public function cariDataMahasiswa ()
        {   
            
            $keyword = $this->input->post('keyword', true);
            
            $this->db->like('nama',$keyword);
            $this->db->or_like('id_pelanggan',$keyword);
            $this->db->or_like('handphone',$keyword);
            $this->db->or_like('elektronik',$keyword);
            return $this->db->get('customer')->result_array();
        }





 }