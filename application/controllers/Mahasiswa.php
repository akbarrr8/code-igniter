<?php 
    class Mahasiswa extends CI_Controller {


        public function __construct()
        {
            parent ::__construct();
            $this->load->model('Mahasiswa_model');
            $this->load->library('form_validation');

            
        } 
        
        // menggunakan config --> autoload /akan terload semua class function

        public function index ()
        {
            
            $data['judul'] = 'Data Mahasiswa';

            $data ['customer'] = $this ->Mahasiswa_model->getAllMahasiswa();
            if ($this->input->post('keyword') ){
                $data['customer'] = $this ->Mahasiswa_model->cariDataMahasiswa();
            }
            $this->load->view('templates/header', $data);
            $this->load->view('mahasiswa/index', $data);
            $this->load->view('templates/footer');


        }

        public function tambah ()
        {
            
            $data['judul'] = 'Tambah Data';
            $this -> form_validation ->set_rules('nama', 'Nama', 'required');
            $this -> form_validation ->set_rules('handphone', 'Handphone', 'required|numeric');
            $this -> form_validation ->set_rules('elektronik', 'Elektronik', 'required');
            if ($this ->form_validation->run() == FALSE ) {
                
                
                $data ['customer'] = $this ->Mahasiswa_model->getAllMahasiswa();
                $this->load->view('templates/header', $data);
                $this->load->view('mahasiswa/tambah');
                $this->load->view('templates/footer');
            }
            else {
                $this->Mahasiswa_model->tambahDataMahasiswa();
                $this->session->set_flashdata('flash', 'Ditambahkan');
                redirect ('mahasiswa');
            }

        }

        public function hapus ($id_pelanggan)

        {
            $this->Mahasiswa_model->hapusMahasiswaData($id_pelanggan);
            $this->session->set_flashdata('flash', 'Dihapus');
            redirect('mahasiswa');
        }

       public function detail ($id_pelanggan)
        {
            
            $data['judul'] = 'Detail Pelanggan';
            $data ['customer'] = $this ->Mahasiswa_model->getMahasiswaById($id_pelanggan);
            $this->load->view('templates/header', $data);
            $this->load->view('mahasiswa/detail', $data);
            $this->load->view('templates/footer');


        }


        public function ubah ($id_pelanggan)
        {
            
            $data['judul'] = 'Ubah Data';
            $data['status'] = ['Antrian', 'Dalam Pengerjaan', 'selesai'];
            $data ['customer'] = $this->Mahasiswa_model->getMahasiswaById($id_pelanggan);
            $this -> form_validation ->set_rules('nama', 'Nama', 'required');
            $this -> form_validation ->set_rules('handphone', 'Handphone', 'required|numeric');
            $this -> form_validation ->set_rules('elektronik', 'Elektronik', 'required');

            if ($this ->form_validation->run() == FALSE ) 
            {  
                // $data ['customer'] = $this ->Mahasiswa_model->ubahDataMahasiswa($id_pelanggan);
                
                $this->load->view('templates/header', $data);
                $this->load->view('mahasiswa/ubah', $data);
                $this->load->view('templates/footer');
            }
            else 
            {
                $this->Mahasiswa_model->ubahDataMahasiswa();
                $this->session->set_flashdata('flash', 'Data Diubah');
                redirect ('mahasiswa');
            }

        }


    }
